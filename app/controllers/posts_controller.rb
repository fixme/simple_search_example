class PostsController < ApplicationController
  before_action :find_post, only: [:edit, :update, :destroy]

  def index
    @search = Post.search query_params[:query]
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.create post_params
    redirect_to action: :index
  end

  def edit
  end

  def update
    @post.update_attributes post_params
    redirect_to action: :edit
  end

  def destroy
    @post.destroy
    redirect_to action: :index
  end

  private

  def find_post
    @post = Post.find params[:id].to_i
  end

  def post_params
    params.require(:post).permit(:title, :body)
  end

  def query_params
    params.permit(:query)
  end
end
