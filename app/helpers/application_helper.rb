module ApplicationHelper
  def highlight_text(text, terms = [])
    if terms.present?
      text.gsub /(#{ terms.join('|')})/i do |term|
        content_tag :span, class: 'label label-warning' do
          term
        end
      end.html_safe
    end || text
  end

  def anchor_links(count)
    content_tag :ul, class: :pagination do
      count.times.map do |i|
        anchor = i + 1
        content_tag :li do
          link_to anchor.to_s, "##{anchor}"
        end
      end.join.html_safe
    end
  end
end
