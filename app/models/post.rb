class Post < ActiveRecord::Base
  include SimpleSearch::Model

  mapping do |i|
    i.indexes :title, boost: 1.5
    i.indexes :body, analyzer: SimpleSearch::Analyzer::Standard
  end
  
end
