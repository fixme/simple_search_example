class CreateSimpleSearchFields < ActiveRecord::Migration
  def change
    create_table :simple_search_fields do |t|
      t.references :source, polymorphic: true, index: true
      t.string :name
      t.float :boost, default: 1.0

      t.timestamps
    end
    add_index :simple_search_fields, :name
  end
end
