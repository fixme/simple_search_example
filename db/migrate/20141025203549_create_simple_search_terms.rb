class CreateSimpleSearchTerms < ActiveRecord::Migration
  def change
    create_table :simple_search_terms do |t|
      t.references :field, index: true
      t.string :value
      t.float :relevance, default: 1.0

      t.timestamps
    end
    add_index :simple_search_terms, :value
  end
end
