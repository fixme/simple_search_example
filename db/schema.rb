# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141025233422) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "posts", force: true do |t|
    t.string   "title"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "simple_search_fields", force: true do |t|
    t.integer  "source_id"
    t.string   "source_type"
    t.string   "name"
    t.float    "boost",       default: 1.0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "simple_search_fields", ["name"], name: "index_simple_search_fields_on_name", using: :btree
  add_index "simple_search_fields", ["source_id", "source_type"], name: "index_simple_search_fields_on_source_id_and_source_type", using: :btree

  create_table "simple_search_terms", force: true do |t|
    t.integer  "field_id"
    t.string   "value"
    t.float    "relevance",  default: 1.0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "simple_search_terms", ["field_id"], name: "index_simple_search_terms_on_field_id", using: :btree
  add_index "simple_search_terms", ["value"], name: "index_simple_search_terms_on_value", using: :btree

end
