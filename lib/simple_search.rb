# library main module to require
module SimpleSearch
  require 'simple_search/config'
  require 'simple_search/index'
  require 'simple_search/resource'
  require 'simple_search/analyzer'
  require 'simple_search/mapping'
  require 'simple_search/search'
  # activerecord extensions
  require 'simple_search/model'
  # models
  require 'simple_search/field'
  require 'simple_search/field/term'

  def self.table_name_prefix
    'simple_search_'
  end

  def self.configure
    yield config if block_given?
  end

  def self.config
    Config.instance
  end
end
