module SimpleSearch
  # Main analyzer module
  # All analyzers should be required here
  module Analyzer
    require 'simple_search/analyzer/standard'
  end
end
