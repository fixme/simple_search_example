module SimpleSearch
  module Analyzer
    # Default analyzer
    class Standard
      attr_reader :value

      def initialize(value)
        @value = value.to_s
      end

      def analyze
        @analyze ||= begin
          terms = Hash.new
          @value.downcase.scan(/\w+/).each do |term|
            unless stop_words.include? term
              terms[term] ||= 0
              terms[term] += 1
            end
          end
          terms
        end
      end

      def terms
        @terms ||= analyze.keys
      end

      private

      def stop_words
        SimpleSearch.config.stop_words || []
      end
    end
  end
end
