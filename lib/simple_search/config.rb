module SimpleSearch
  # Library configuration singleton
  class Config
    STOP_WORDS = %w( i a about an are as at be by com for from
                     how in is it of on or that the this to was
                     what when where who will with the www )

    attr_accessor :stop_words

    def self.instance
      @instance ||= new
    end

    def initialize
      @stop_words = STOP_WORDS
    end
  end
end
