module SimpleSearch
  # Index field model
  class Field < ActiveRecord::Base
    belongs_to :source, polymorphic: true
    has_many :terms, dependent: :delete_all
  end
end
