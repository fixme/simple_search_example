module SimpleSearch
  # Index term model
  class Term < ActiveRecord::Base
    belongs_to :field
  end
end
