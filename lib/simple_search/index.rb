module SimpleSearch
  # Index class
  # available as Model.index
  class Index
    attr_reader :model

    def initialize(model, &block)
      @model = model
      @mapping = block_given? ? mapping(&block) : Mapping::Fieldset.new
    end

    def search(query)
      SimpleSearch::Search.new(model, query)
    end

    def import(options = {})
      model.find_each do |object|
        object.index.import options
      end if mapping.present?
    end

    def mapping
      yield @mapping = Mapping::Fieldset.new if block_given?
      @mapping
    end
  end
end
