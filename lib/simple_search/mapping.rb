module SimpleSearch
  # Main mapping module
  module Mapping
    require 'simple_search/mapping/fieldset'
    require 'simple_search/mapping/field'
  end
end
