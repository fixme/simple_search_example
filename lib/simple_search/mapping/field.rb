module SimpleSearch
  module Mapping
    # Index mapping field class
    class Field
      attr_reader :name, :analyzer, :boost

      def initialize(name, options = {})
        @name = name
        @analyzer = options[:analyzer] || SimpleSearch::Analyzer::Standard
        @boost = options[:boost] || 1.0
      end

      def analyze(value)
        analyzer.new(value).analyze
      end

      def import(source)
        terms = []
        source.transaction do
          value = source.send name
          field = indexed_field_for source
          field.terms.delete_all
          terms = analyze(value).map do |term, relevance|
            field.terms.create! value: term, relevance: relevance * field.boost
          end
        end if source.present? && source.persisted?

        terms
      end

      def indexed_field_for(source)
        attrs = { name: name }
        field = source.indexed_fields.find_by(attrs) ||
                source.indexed_fields.new(attrs)
        field.tap do |f|
          f.boost = boost
          f.save!
        end
      end
    end
  end
end
