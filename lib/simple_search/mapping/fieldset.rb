module SimpleSearch
  module Mapping
    # Index mapping fieldset
    # available as Model.mapping
    # usage examples:
    # Model.mapping do |i|
    #   i.indexed :field_name
    # end
    class Fieldset
      attr_reader :fields
      delegate :present?, :blank?, to: :fields

      def initialize(*fields)
        @fields = fields
      end

      def indexes(field_name, options = {})
        @fields << Mapping::Field.new(field_name, options)
      end

      def method_missing(method, *args, &block)
        fields.send method, *args, &block
      end
    end
  end
end
