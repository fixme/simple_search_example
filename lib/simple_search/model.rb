module SimpleSearch
  # Mixins for ActiveRecord models
  module Model
    def self.included(base)
      base.send :extend, ClassMethods
      base.send :include, InstanceMethods
      # association & callbacks
      base.has_many :indexed_fields, class_name: '::SimpleSearch::Field',
                                     as: :source, dependent: :destroy
      base.after_save :update_index, if: :changed?
      base.after_destroy :delete_index
    end

    # ActiveRecord class methods
    module ClassMethods
      def mapping
        if block_given?
          yield index.mapping
        else
          index.mapping
        end
      end

      def index
        @_index_ ||= SimpleSearch::Index.new self
      end

      def search(query)
        index.search query
      end
    end

    # ActiveRecord instance methods
    module InstanceMethods
      def index
        @_index_ ||= SimpleSearch::Resource.new self
      end

      def update_index
        index.import if index.mapping.present?
      end

      def delete_index
        index.delete
      end
    end
  end
end
