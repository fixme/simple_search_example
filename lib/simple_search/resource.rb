module SimpleSearch
  # Resource index class
  class Resource
    attr_reader :source

    def initialize(source)
      @source = source
    end

    def index
      source.class.index
    end

    def mapping
      index.mapping
    end

    def import(options = {})
      delete if options[:force]
      mapping.map do |field|
        field.import source
      end
    end

    def delete
      source.indexed_fields.destroy_all
    end

    def create
      import force: true
    end
  end
end
