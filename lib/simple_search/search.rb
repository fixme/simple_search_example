module SimpleSearch
  # Searching class
  class Search
    attr_accessor :model, :query, :terms, :results

    def initialize(model, query)
      @model = model
      @query = query
      @terms = Analyzer::Standard.new(query).terms
      @results = if terms.present? 
        model.joins(indexed_fields: :terms)
          .select("#{ model.table_name }.*,
                   sum(#{ terms_table_name }.relevance) as relevance")
          .order('relevance DESC')
          .group("#{ model.table_name }.id")
          .where(terms_table_name => { value: terms })
        else
          model.all
        end
    end

    def method_missing(method, *args, &block)
      results.send method, *args, &block
    end

    private

    def terms_table_name
      @terms_table_name ||= SimpleSearch::Term.table_name
    end
  end
end
