require 'rails_helper'

RSpec.describe SimpleSearch::Analyzer::Standard, type: :lib do
  let(:config) { double :config, stop_words: ['to', 'or']}
  let(:value) { 'To BE or ;noT, to be!' }

  subject { SimpleSearch::Analyzer::Standard.new value }

  before :each do
    allow(SimpleSearch).to receive(:config).and_return(config)
  end

  describe '#initialize' do
    let(:value) { :test_value }

    it 'should set #value as a string' do
      expect(subject.value).to eq 'test_value'
    end
  end

  describe '#analyze' do
    it 'should split the text to words, reject stop words and count revelance' do
      expect(subject.analyze).to eq 'be' => 2, 'not' => 1
    end
  end


  describe '#terms' do
    it 'should return unique terms from #words list and cache the results' do
      allow(subject).to receive(:analyze).once.and_return('be' => 2, 'not' => 1)

      expect(subject.terms).to eq ['be', 'not']
      subject.terms
    end
  end

  context 'private methods' do
    describe '#stop_words' do
      it 'should return a list of stop words' do
        expect(
          subject.send :stop_words
        ).to eq config.stop_words
      end
    end
  end
end
