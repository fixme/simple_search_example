require 'rails_helper'

RSpec.describe SimpleSearch::Config, type: :lib do

  subject { SimpleSearch::Config }

  describe '::instance' do
    it 'should return instance' do
      expect(subject.instance).to be_instance_of subject
    end

    it 'should always return the same instance' do
      expect(subject.instance).to eq subject.instance
    end
  end

  describe '#initialize' do
    it 'should set #stop_words to default array' do
      instance = subject.new
      expect(instance.stop_words).to be_kind_of Array
    end
  end
end
