require 'rails_helper'

RSpec.describe SimpleSearch::Index, type: :lib do
  let(:model) { double :model }
  let(:record) { double :record }
  let(:mapping) { double :mapping, present?: true }

  subject { SimpleSearch::Index.new model }

  before :each do
    allow(SimpleSearch::Mapping::Fieldset).to receive(:new).and_return(mapping)
  end

  describe '#initialize' do
    it 'should set #model' do
      expect(subject.model).to eq model
    end

    it 'should set #mapping' do
      expect(subject.mapping).to eq mapping
    end

    context 'with block' do
      subject { SimpleSearch::Index }

      it 'should pass block to #mapping if block given' do
        expect(mapping).to receive(:test_method).with(:test_arg)
        subject.new(model) do |i|
          i.test_method :test_arg
        end
      end
    end
  end

  describe '#mapping' do
    it 'should yield with new mapping instance if block given' do
      expect do |block|
        subject.mapping(&block)
      end.to yield_with_args(mapping)
    end
  end

  describe '#search' do
    let(:search) { double :search }
    let(:query) { double :query }

    it 'should return search instance' do
      allow(SimpleSearch::Search).to receive(:new).with(model, query).and_return(search)

      expect(
        subject.search(query)
      ).to eq search
    end
  end

  describe '#import' do
    let(:index) { double :index }
    let(:options) { double :options }

    it 'should call #index.import for each record found' do
      expect(model).to receive(:find_each) do |&block|
        block.call(record)
      end
      expect(record).to receive(:index).and_return(index)
      expect(index).to receive(:import).with(options)

      subject.import options
    end
  end
end
