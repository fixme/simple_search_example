require 'rails_helper'

RSpec.describe SimpleSearch::Mapping::Field, type: :lib do

  subject { SimpleSearch::Mapping::Field.new :field_name }

  describe '#initialize' do
    it 'should set #name' do
      expect(subject.name).to eq :field_name
    end

    it 'should set #analyzer by default' do
      expect(subject.analyzer).to eq SimpleSearch::Analyzer::Standard
    end

    it 'should set #boost by default' do
      expect(subject.boost).to eq 1.0
    end

    context 'with options' do
      let(:analyzer) { double :analyzer }

      subject { SimpleSearch::Mapping::Field.new :field_name, analyzer: analyzer, boost: 2.5 }

      it 'should set #analyzer' do
        expect(subject.analyzer).to eq analyzer
      end

      it 'should set #boost' do
        expect(subject.boost).to eq 2.5
      end
    end
  end

  describe '#analyze' do
    let(:analyzer) { double :analyzer }

    it 'should build analyzer instance and retrieve #analyze from it' do
      expect(subject.analyzer).to receive(:new).with(:argument_example).and_return(analyzer)
      expect(analyzer).to receive(:analyze)
      subject.analyze :argument_example
    end
  end

  describe '#import' do
    let(:document) { double :document, persisted?: true, present?: true, field_name: 'example text example' }
    let(:field) { double :field, terms: terms, boost: 1.5 }
    let(:terms) { double :terms }

    after :each do
      subject.import document
    end

    it 'should recreate all document\'s terms' do
      expect(document).to receive(:transaction) do |&block|
        block.call
      end
      expect(subject).to receive(:indexed_field_for).with(document).and_return(field)
      expect(subject).to receive(:analyze).with(document.field_name).and_return({'example' => 2, 'text' => 1})
      expect(terms).to receive(:delete_all)
      expect(terms).to receive(:create!).with(value: 'example', relevance: 3.0)
      expect(terms).to receive(:create!).with(value: 'text', relevance: 1.5)
    end

    it 'should not do anything if document isn\'t persisted' do
      allow(document).to receive(:persisted?).and_return(false)
      expect(document).not_to receive(:transaction)
    end
  end

  describe '#indexed_field_for' do
    let(:document) { double :document, indexed_fields: fields_collection }
    let(:field) { double :field }
    let(:fields_collection) { double :fields_collection }

    it 'should try to find field record and update #boost' do
      expect(fields_collection).to receive(:find_by).with(name: :field_name).and_return(field)
      expect(fields_collection).not_to receive(:new)
      expect(field).to receive(:boost=).with(1.0)
      expect(field).to receive(:save!)

      subject.indexed_field_for document
    end

    it 'should try to create new field record if it doesn\'t exist and set #boost' do
      expect(fields_collection).to receive(:find_by).with(name: :field_name).and_return(nil)
      expect(fields_collection).to receive(:new).and_return(field)
      expect(field).to receive(:boost=).with(1.0)
      expect(field).to receive(:save!)

      subject.indexed_field_for document
    end
  end
end
