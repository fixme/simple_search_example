require 'rails_helper'

RSpec.describe SimpleSearch::Mapping::Fieldset, type: :lib do
  subject { SimpleSearch::Mapping::Fieldset.new }

  describe '#initialize' do
    subject { SimpleSearch::Mapping::Fieldset.new 1, 2, 3 }

    it 'should set #field' do
      expect(subject.fields).to eq [1, 2, 3]
    end
  end

  describe '#indexes' do
    let(:name) { :test_field }
    let(:options) { Hash.new }
    let(:field) { double :field, name: name, options: options }

    it 'should add field to #fields' do
      expect(SimpleSearch::Mapping::Field).to receive(:new).with(name, options).and_return(field)
      subject.indexes name, options
      expect(subject.fields).to eq [field]
    end
  end

  describe '#method_missing' do
    let(:fields) { double :fields }

    it 'should delegate every missing method to #fields' do
      expect(subject).to receive(:fields).and_return(fields)
      expect(fields).to receive(:test_method)
      subject.test_method
    end
  end

  describe 'delegations' do
    it 'should delegate #present? to #fields' do
      expect(subject.present?).not_to be
    end

    it 'should delegate #blank? to #fields' do
      expect(subject.blank?).to be
    end
  end
end
