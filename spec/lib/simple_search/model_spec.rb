require 'rails_helper'

RSpec.describe SimpleSearch::Model, type: :lib do

  class ModelInterfaceMock
    def self.has_many(*args); end
    def self.after_save(*args); end
    def self.after_destroy(*args); end
  end

  let(:mixin) { SimpleSearch::Model }

  subject { Class.new(ModelInterfaceMock) }

  context 'associations and callbacks' do
    after :each do
      subject.send :include, mixin
    end

    it 'should add #indexed_fields association' do
      expect(subject).to receive(:has_many)
        .with(:indexed_fields, class_name: '::SimpleSearch::Field', as: :source, dependent: :destroy)
    end

    it 'should add #update_index to after_save callbacks' do
      expect(subject).to receive(:after_save).with(:update_index, if: :changed?)
    end

    it 'should add #destroy_index to after_destroy callbacks' do
      expect(subject).to receive(:after_destroy).with(:delete_index)
    end
  end

  context 'instance methods' do
    let(:instance) { subject.new }
    let(:resource) { double :resource, mapping: true }

    before :each do
      subject.send :include, mixin
    end

    describe '#index' do
      it 'should build an index resource and cache it' do
        expect(SimpleSearch::Resource).to receive(:new).with(subject).once.and_return(resource)
        expect(instance.index).to eq resource
        instance.index # it shouldn't call resource.new again
      end
    end

    describe '#update_index' do
      it 'should be delegated to #index' do
        expect(instance).to receive(:index).twice.and_return(resource)
        expect(resource).to receive(:import)
        instance.update_index
      end

      it 'should not do anything if mapping isn\'t present' do
        expect(instance).to receive(:index).once.and_return(resource)
        expect(resource).to receive(:mapping).and_return(false)
        expect(resource).not_to receive(:import)
        instance.update_index
      end
    end

    describe '#destroy_index' do
      it 'should be delegated to #index' do
        expect(instance).to receive(:index).and_return(resource)
        expect(resource).to receive(:delete)
        instance.delete_index
      end
    end
  end

  context 'class methods' do
    let(:index) { double :index }

    before :each do
      subject.send :include, mixin
    end

    describe '::index' do
      it 'should build an index resource and cache it' do
        expect(SimpleSearch::Index).to receive(:new).with(subject).once.and_return(index)
        expect(subject.index).to eq index
        subject.index # it shouldn't call resource.new again
      end
    end

    describe '::search' do
      it 'should be delegated to #index' do
        expect(subject).to receive(:index).and_return(index)
        expect(index).to receive(:search).with :test_query
        subject.search :test_query
      end
    end

    describe '::mapping' do
      let(:mapping) { double :mapping }
      let(:index) { double :index, mapping: mapping }

      before :each do
        allow(subject).to receive(:index).and_return(index)
      end

      it 'should return ::index.mapping' do
        expect(subject.mapping).to eq mapping
      end

      it 'should yield with ::index.mapping if block given' do
        expect do |block|
          subject.mapping(&block)
        end.to yield_with_args(mapping)
      end
    end
  end
end
