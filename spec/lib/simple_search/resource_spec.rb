require 'rails_helper'

RSpec.describe SimpleSearch::Resource, type: :lib do
  let(:document) { double :document, indexed_fields: fields_collection, class: document_class }
  let(:document_class) { double :document_class }
  let(:fields_collection) { double :fields_collection }

  subject { SimpleSearch::Resource.new document }

  describe '#initialize' do
    it 'should set #source' do
      expect(subject.source).to eq document
    end
  end

  describe '#index' do
    it 'should be delegated to source class' do
      expect(document_class).to receive(:index)
      subject.index
    end
  end

  describe '#mapping' do
    let(:index) { double :index }
    it 'should be delegated to #index' do
      expect(subject).to receive(:index).and_return(index)
      expect(index).to receive(:mapping)
      subject.mapping
    end
  end

  describe '#import' do
    let(:field_1) { double :field_1 }
    let(:field_2) { double :field_2 }

    it 'should call #delete if force options is defined' do
      expect(subject).to receive(:delete)
      expect(subject).to receive(:mapping).and_return([])
      subject.import force: true
    end

    it 'should call #import for each retrieved field' do
      expect(subject).to receive(:mapping).and_return([field_1, field_2])
      expect(field_1).to receive(:import).with(document)
      expect(field_2).to receive(:import).with(document)
      subject.import
    end
  end

  describe '#delete' do
    it 'should call #destroy_all on fields collection' do
      expect(fields_collection).to receive(:destroy_all)
      subject.delete
    end
  end

  describe '#create' do
    it 'should call #import with force option' do
      expect(subject).to receive(:import).with force: true
      subject.create
    end
  end
end
