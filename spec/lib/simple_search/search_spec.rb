require 'rails_helper'

RSpec.describe SimpleSearch::Search, type: :lib do

  class ModelInterfaceMock
    def self.joins(*); self; end
    def self.select(*); self; end
    def self.order(*); self; end
    def self.group(*); self; end
    def self.where(*); self; end
    def self.table_name; end
  end

  let(:model) { Class.new(ModelInterfaceMock) }
  let(:analyzer) { double :analyzer, :terms => :terms }

  subject { SimpleSearch::Search.new model, :query_example }

  describe '#initialize' do
    it 'should set #model' do
      expect(subject.model).to eq model
    end

    it 'should set #query' do
      expect(subject.query).to eq :query_example
    end

    it 'should split query to terms using analyzer' do
      expect(SimpleSearch::Analyzer::Standard).to receive(:new).with(:query_example).and_return(analyzer)
      expect(subject.terms).to eq :terms
    end

    it 'should build a relation as #results' do
      expect(subject.results).to eq model
    end
  end

  describe '#results' do
    let(:model) { Post }

    context 'with real model' do
      it 'shouldn\'t raise error' do
        expect do
          subject.results.load
        end.not_to raise_error
      end

      it 'should return ActiveRecord relation' do
        expect(subject.results.load).to be_a ActiveRecord::Relation
      end
    end
  end

  describe '#terms_table_name' do
    it 'should return terms table name' do
      expect(SimpleSearch::Term).to receive(:table_name).and_return(:example_table_name)
      expect(
        subject.send :terms_table_name
      ).to eq :example_table_name
    end
  end
end
