require 'rails_helper'

RSpec.describe SimpleSearch, type: :lib do
  subject { SimpleSearch }
  let(:config) { double :config }

  describe '::config' do
    it 'should return SimpleSearch::Config#instance' do
      allow(SimpleSearch::Config).to receive(:instance).and_return(config)
      expect(subject.config).to eq config
    end
  end

  describe '::configure' do
    it 'should yield with ::config' do
      allow(subject).to receive(:config).and_return(config)
      expect do |block|
        subject.configure(&block)
      end.to yield_with_args(config)
    end
  end
end
